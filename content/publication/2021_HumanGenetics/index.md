---
abstract: Recent studies have shown the ubiquity of pleiotropy for variants affecting human complex traits. These studies also show
  that rare variants tend to be less pleiotropic than common ones, suggesting that purifying natural selection acts against highly
  pleiotropic variants of large effect. Here, we investigate the mean frequency, effect size and recombination rate associated
  with pleiotropic variants, and focus particularly on whether highly pleiotropic variants are enriched in regions with putative
  strong background selection. We evaluate variants for 41 human traits using data from the NHGRI-EBI GWAS Catalog, as
  well as data from other three studies. Our results show that variants involving a higher degree of pleiotropy tend to be more
  common, have larger mean effect sizes, and contribute more to heritability than variants with a lower degree of pleiotropy.
  This is consistent with the fact that variants of large effect and frequency are more likely detected by GWAS. Using data
  from four different studies, we also show that more pleiotropic variants are enriched in genome regions with stronger back-
  ground selection than less pleiotropic variants, suggesting that highly pleiotropic variants are subjected to strong purifying
  selection. From the above results, we hypothesized that a number of highly pleiotropic variants of low effect/frequency may
  pass undetected by GWAS.
authors:
- admin
- Eugenio López-Cortegano
date: "2021-07-06"
doi: "10.1007/s00439-021-02308-w"
featured: false
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Human Genetics*, 140'
publication_short: ""
publication_types:
- "2"
publishDate: "2021-07-06"
summary: We investigate the association between pleiotropy degree and the strength of background selection using data from the GWAS Catalog.
tags:
- Heritability
- Human data
title: Highly pleiotropic variants of human traits are enriched in genomic regions with strong background selection
url_code: ""
url_dataset: ""
url_pdf: https://link.springer.com/content/pdf/10.1007/s00439-021-02308-w.pdf
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
