---
abstract: Genetic variation originates from several types of spontaneous mutation, including single-nucleotide substitutions, short
  insertions and deletions (indels), and larger structural changes. Structural mutations (SMs) drive genome evolution and
  are thought to play major roles in evolutionary adaptation, speciation, and genetic disease, including cancers.
  Sequencing of mutation accumulation (MA) lines has provided estimates of rates and spectra of single-nucleotide and indel
  mutations in many species, yet the rate of new SMs is largely unknown. Here, we use long-read sequencing to determine the
  full mutation spectrum in MA lines derived from two strains (CC-1952 and CC-2931) of the green alga *Chlamydomonas*
  *reinhardtii*. The SM rate is highly variable between strains and between MA lines, and SMs represent a substantial proportion
  of all mutations in both strains (CC-1952 6%; CC-2931 12%). The SM spectra differ considerably between the two strains,
  with almost all inversions and translocations occurring in CC-2931 MA lines. This variation is associated with heterogeneity
  in the number and type of active transposable elements (TEs), which comprise major proportions of SMs in both strains
  (CC-1952 22%; CC-2931 38%). In CC-2931, a Crypton and a previously undescribed type of DNA element have caused
  71% of chromosomal rearrangements, whereas in CC-1952, a Dualen LINE is associated with 87% of duplications. Other
  SMs, notably large duplications in CC-2931, are likely products of various double-strand break repair pathways. Our results
  show that diverse types of SMs occur at substantial rates, and support prominent roles for SMs and TEs in evolution.

authors:
- admin
date: "2023-02-02"
doi: "https://doi.org/10.1101/gr.276957.122 "
featured: true
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Genome Research*, 33: 45-60.'
publication_short: ""
publication_types:
- "2"
publishDate: "2023-02-02"
summary: The first study on *de novo* structural mutations (SMs) using long-read sequencing data. Resuts in MA lines of two different strains of *C. reinhardtii* show that SMs occur at substantial rates. Major differences between the SM spectra can be largely attributed to differences in the spectra of active transposable elements.
tags:
- Chlamydomonas reinhardtii
- Mutation
title: Rates and spectra of de novo structural mutations in Chlamydomonas reinhardtii
url_code: ""
url_dataset: "https://www.ncbi.nlm.nih.gov/bioproject/?term=PRJNA839925"
url_pdf: "https://genome-cshlp-org.ezproxy.is.ed.ac.uk/content/33/1/45.full.pdf"
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
