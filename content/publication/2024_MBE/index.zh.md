---
abstract: The mouse serves as a mammalian model for understanding the nature of variation from new mutations, a question
  that has both evolutionary and medical significance. Previous studies suggest that the rate of single nucleotide mutations 
  (SNMs) in mice is approximately 50% of that in humans. However, information largely comes from studies involving the
  C57BL/6 strain, and there is little information from other mouse strains. Here, we study the mutations that accumulated
  in 59 mouse lines derived from four inbred strains that are commonly used in genetics and clinical research (BALB/cAnNRj, 
  C57BL/6JRj, C3H/HeNRj, and FVB/NRj), maintained for 8-9 generations by brother-sister mating. By analysing Illumina
  whole-genome sequencing data, we estimate that the average rate of new SNMs in mice is approximately $\mu = 6.7 \times 10^{-9}$. 
  However, there is substantial variation in the spectrum of SNMs among strains, so the burden from new mutations also 
  varies among strains. For example, the FVB strain has a spectrum that is markedly skewed towards C→A transversions, and 
  is likely to experience a higher deleterious load than other strains, due to an increased frequency of nonsense mutations 
  in glutamic acid codons. Finally, we observe substantial variation in the rate of new SNMs among DNA sequence contexts, 
  CpG sites and their adjacent nucleotides playing an important role.

authors:
- admin
date: "2024-07-31"
doi: "https://doi.org/10.1093/molbev/msae163"
featured: false
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Molecular Biology and Evolution*'
publication_short: ""
publication_types:
- "2"
publishDate: "2024-07-31"
summary: 我们验证了以前的小鼠基因组突变率，并证明小鼠品系之间突变谱的变化导致适应度效应分布的差异。
tags:
- Mus musculus
- 突变
title: Variation in the spectrum of new mutations among inbred strains of mice
url_code: ""
url_dataset: "https://www.ncbi.nlm.nih.gov/bioproject/?term=PRJNA1017978"
url_pdf: "https://academic.oup.com/mbe/advance-article-pdf/doi/10.1093/molbev/msae163/58735313/msae163.pdf"
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
