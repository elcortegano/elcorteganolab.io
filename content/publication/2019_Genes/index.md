---
abstract:  During the last decade, there has been a huge development of Genome-Wide Association
  Studies (GWAS), and thousands of loci associated to complex traits have been detected. These efforts
  have led to the creation of public databases of GWAS results, making a huge source of information
  available on the genetic background of many diverse traits. Here we present GWEHS (Genome-Wide
  Effect size and Heritability Screener), an open-source online application to screen loci associated to
  human complex traits and diseases from the NHGRI-EBI GWAS Catalog. This application provides a
  way to explore the distribution of effect sizes of loci affecting these traits, as well as their contribution
  to heritability. Furthermore, it allows for making predictions on the change in the expected mean effect
  size, as well as in the heritability as new loci are found. The application enables inferences on whether
  the additive contribution of loci expected to be discovered in the future will be able to explain the
  estimates of familial heritability for the different traits. We illustrate the use of this tool, compare some
  of the results obtained with those from a previous meta-analysis, and discuss its uses and limitations.
authors:
- admin
- Eugenio López-Cortegano
date: "2019-07-24"
doi: "https://doi.org/10.3390/genes10080558"
featured: false
image:
  caption: '[**GWEHS website**](http://gwehs.uvigo.es)'
  focal_point: ""
  preview_only: false
projects: []
publication: '*Genes*, 10(8)'
publication_short: ""
publication_types:
- "2"
publishDate: "2019-07-24"
summary: We develop a tool that process data from the GWAS Catalog and performs analyses on the loci effect size distributions and contributions to heritability. The tool is accessible via an interactive web site (http://gwehs.uvigo.es), and a docker image.
tags:
- Heritability
- Human data
- Software
title: "GWEHS: A Genome-Wide Effect Sizes and Heritability Screener"
url_code: "https://gitlab.com/elcortegano/GWEHS"
url_dataset: "https://www.ebi.ac.uk/gwas/"
url_pdf: https://www.mdpi.com/2073-4425/10/8/558/pdf
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
