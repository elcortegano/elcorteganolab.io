---
abstract: All forms of genetic variation originate from new mutations, making it crucial to understand their rates and mechanisms.
  Here, we use long-read PacBio sequencing to investigate de novo mutations that accumulated in 12 inbred mouse lines derived
  from three commonly used inbred strains (C3H, C57BL/6, and FVB) maintained for 8-15 generations in a mutation accumulation (MA)
  experiment. We built chromosome-level genome assemblies based on the MA line founders' genomes, and then employed a combination
  of read and assembly-based methods to call the complete spectrum of new mutations. On average, there are ~45 mutations per
  haploid genome per generation, about half of which (54%) are insertions and deletions shorter than 50 bp (indels). The remainder
  are single nucleotide mutations (SNMs, 44%) and large structural mutations (SMs, 2%). We found that the degree of DNA repetitiveness
  is positively correlated with SNM and indel rates, and that a substantial fraction of SMs can be explained by homology-dependent
  mechanisms associated with repeat sequences. Most (90%) indels can be attributed to microsatellite contractions and expansions,
  and there is a marked bias towards 4 bp indels. Among the different types of SMs, tandem repeat mutations have the highest mutation
  rate, followed by insertions of transposable elements (TEs). We uncover a rich landscape of active TEs, and notable differences
  in their spectrum among MA lines and strains, and a high rate of gene retroposition. Our study offers novel insights into
  mammalian genome evolution, and highlights the importance of repetitive elements in shaping genomic diversity. 

authors:
- admin
date: "2025-01-22"
doi: "10.1101/gr.279982.124 "
featured: true
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Genome Research*'
publication_short: ""
publication_types:
- "2"
publishDate: "2025-01-22"
summary: We estimate for the first time the rate and spectrum of de novo mutations in mice using long-read sequencing, uncovering an important role of indels and transposable element insertions in generating new genetic variation.
tags:
- Mus musculus
- Mutation
title: The rate and spectrum of new mutations in mice inferred by long-read sequencing
url_code: ""
url_dataset: "https://www.ncbi.nlm.nih.gov/bioproject/?term=PRJNA1112615"
url_pdf: "https://genome.cshlp.org/content/35/1/43.full.pdf"
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
