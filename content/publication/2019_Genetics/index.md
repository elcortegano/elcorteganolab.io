---
abstract: Thousands of genes responsible for many diseases and other common traits in humans have been detected by Genome
  Wide Association Studies (GWAS) in the last decade. However, candidate causal variants found so far usually explain only a small
  fraction of the heritability estimated by family data. The most common explanation for this observation is that the missing heritability
  corresponds to variants, either rare or common, with very small effect, which pass undetected due to a lack of statistical power. We
  carried out a meta-analysis using data from the NHGRI-EBI GWAS Catalog in order to explore the observed distribution of locus effects
  for a set of 42 complex traits and to quantify their contribution to narrow-sense heritability. With the data at hand, we were able to
  predict the expected distribution of locus effects for 16 traits and diseases, their expected contribution to heritability, and the missing
  number of loci yet to be discovered to fully explain the familial heritability estimates. Our results indicate that, for 6 out of the 16 traits,
  the additive contribution of a great number of loci is unable to explain the familial (broad-sense) heritability, suggesting that the gap
  between GWAS and familial estimates of heritability may not ever be closed for these traits. In contrast, for the other 10 traits, the
  additive contribution of hundreds or thousands of loci yet to be found could potentially explain the familial heritability estimates, if this
  were the case. Computer simulations are used to illustrate the possible contribution from nonadditive genetic effects to the gap
  between GWAS and familial estimates of heritability.
authors:
- admin
- Eugenio López-Cortegano
date: "2019-07-01"
doi: "https://doi.org/10.1534/genetics.119.302077"
featured: false
image:
  caption: 'Image credit: [**https://www.embl.org/**](https://www.ebi.ac.uk/gwas/)'
  focal_point: ""
  preview_only: false
projects: []
publication: '*Genetics*, 212(3)'
publication_short: ""
publication_types:
- "2"
publishDate: "2019-07-01"
summary: Using data from the GWAS Catalog, we infer that the loci effect sizes follow a log-normal distribution, and the heritability gap may never be closed for many quantitative traits.
tags:
- Heritability
- Human data
title: Inferring the nature of missing heritability in human traits using data from the GWAS Catalog
url_code: ""
url_dataset: ""
url_pdf: https://www.genetics.org/content/genetics/212/3/891.full.pdf
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
