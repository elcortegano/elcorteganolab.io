---
abstract: Inbreeding depression, the decline in fitness of inbred individuals, is a ubiquitous phenomenon of great relevance in evolutionary
  biology and in the fields of animal and plant breeding and conservation. Inbreeding depression is due to the expression of recessive
  deleterious alleles that are concealed in heterozygous state in noninbred individuals, the so-called inbreeding load. Genetic purging
  reduces inbreeding depression by removing these alleles when expressed in homozygosis due to inbreeding. It is generally thought
  that fast inbreeding (such as that generated by full-sib mating lines) removes only highly deleterious recessive alleles, while slow
  inbreeding can also remove mildly deleterious ones. However, a question remains regarding which proportion of the inbreeding load
  can be removed by purging under slow inbreeding in moderately large populations. We report results of two long-term slow
  inbreeding Drosophila experiments (125–234 generations), each using a large population and a number of derived lines with effective
  sizes about 1000 and 50, respectively. The inbreeding load was virtually exhausted after more than one hundred generations in large
  populations and between a few tens and over one hundred generations in the lines. This result is not expected from genetic drift
  alone, and is in agreement with the theoretical purging predictions. Computer simulations suggest that these results are consistent
  with a model of relatively few deleterious mutations of large homozygous effects and partially recessive gene action.
authors:
- admin
- Eugenio López-Cortegano
date: "2021-08-17"
doi: "https://doi.org/10.1038/s41437-021-00464-3"
featured: false
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Heredity*, 127(4)'
publication_short: ""
publication_types:
- "2"
publishDate: "2021-08-17"
summary: 这项工作继续了先前对果蝇小种群（$N_{e} \approx 50$）的较长时间的研究。 经过 100 多代实验，测得的近亲繁殖负荷几乎耗尽，与清除理论的预期一致。
tags:
- Drosophila melanogaster
- 基因清除
- 近亲繁殖
title: Long-term exhaustion of the inbreeding load in Drosophila melanogaster
url_code: ""
url_dataset: ""
url_pdf: https://www.nature.com/articles/s41437-021-00464-3.pdf
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
