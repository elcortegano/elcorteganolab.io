---
abstract: Management programmes often have to make decisions based on the analysis of the
  genetic properties and diversity of populations. Expected heterozygosity (or gene
  diversity) and population structure parameters are often used to make recommendations
  for conservation, such as avoidance of inbreeding or migration across subpopulations.
  Allelic diversity, however, can also provide complementary and useful
  information for conservation programmes, as it is highly sensitive to population bottlenecks,
  and is more related to long‐term selection response than heterozygosity.
  Here we present a completely revised and updated re‐implementation of the software
  metapop for the analysis of diversity in subdivided populations, as well as a tool
  for the management and dynamic estimation of optimal contributions in conservation
  programmes. This new update includes computation of allelic diversity for population
  analysis and management, as well as a simulation mode to forecast the consequences
  of taking different management strategies over time. Furthermore, the new implementation
  in C++ includes code optimization and improved memory usage, allowing
  for fast analysis of large data sets including single nucleotide polymorphism markers,
  as well as enhanced cross‐software and cross‐platform compatibility.
authors:
- admin
- Eugenio López-Cortegano
date: "2019-04-02"
doi: "https://doi.org/10.1111/1755-0998.13015"
featured: false
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Molecular Ecology Resources*, 19(4)'
publication_short: ""
publication_types:
- "2"
publishDate: "2019-04-02"
summary: Metapop2 是软件 Metapop 的重新实现，用于估计结构化种群中的基因和等位基因多样性成分，并协助管理建议（https://gitlab.com/elcortegano/metapop2）。
tags:
- 遗传多样性
- 近亲繁殖
- 软件
title: "Metapop2: Re-implementation of software for the analysis and management of subdivided populations using gene and allelic diversity"
url_code: "https://gitlab.com/elcortegano/metapop2"
url_dataset: ""
url_pdf: https://onlinelibrary.wiley.com/doi/pdf/10.1111/1755-0998.13015
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
