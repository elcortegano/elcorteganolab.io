---
abstract: The expected change in fitness under inbreeding due to deleterious recessive alleles
  depends on the amount of inbreeding load harbored by a population, that is, the load of
  deleterious recessive mutations concealed in the heterozygous state, and the opposing
  effect of genetic purging to remove such a load. This change in fitness can be thus
  predicted if an estimate of the inbreeding load and the purging coefficient (the
  parameter that quantifies the amount of purging) are available. These two parameters
  can be estimated in pedigreed populations, as has been shown for populations under
  random mating. A question arises whether these parameters can also be estimated
  under other breeding systems as well as whether they allow accurate prediction of the
  corresponding expected change in fitness. In conservation programs, it is usually
  recommended to preserve genetic diversity by equalizing contributions from parents to
  progeny and avoiding inbred matings. Regular systems of inbreeding have also been
  proposed as breeding conservation strategies to purge the inbreeding load. Using
  computer simulations, we first test a method to jointly estimate the initial inbreeding
  load and the purging coefficient in populations subjected to equalization of parental
  contributions, circular mating, and partial full-sib mating. Then, using the expected values
  of the inbreeding coefficient and the variance of family size, as well as the estimates of
  the inbreeding load and the purging coefficient, we make simple predictions of the
  change in fitness over generations under these breeding systems and compare them
  with simulation results. We discuss how these fitness predictions can help undertaking
  conservation designs under different breeding scenarios.
authors:
- admin
- Eugenio López-Cortegano
date: "2022-06-27"
doi: "https://doi-org.ezproxy.is.ed.ac.uk/10.1111/acv.12804"
featured: false
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Animal Conservation*, https://doi-org.ezproxy.is.ed.ac.uk/10.1111/acv.12804'
publication_short: ""
publication_types:
- "2"
publishDate: "2022-06-27"
summary: "Computer simulations are used to test the estimation of inbreeding-purging parameters in populations under different mating regimes: random mating, circular mating, and equal contributions."
tags:
- Computer simulations
- Genetic purging
- Inbreeding
title: Prediction of fitness under different breeding designs in conservation programs
url_code: ""
url_dataset: ""
url_pdf: https://zslpublications-onlinelibrary-wiley-com.ezproxy.is.ed.ac.uk/doi/pdf/10.1111/acv.12804
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
