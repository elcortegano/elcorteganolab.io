---
abstract: Inbreeding depression for fitness traits is a key issue in evolutionary
  biology and conservation genetics. The magnitude of inbreeding depression, though,
  may critically depend on the efficiency of genetic purging, the elimination or
  recessive deleterious mutations by natural selection after they are exposed by
  inbreeding. However, the detection and quantification of genetic purging for
  nonlethal mutations is a rather difficult task. Here, we present two comprehensive
  sets of experiments with Drosophila aimed at detecting genetic purging in
  competitive conditions and quantifying its magnitude. We obtain, for the first
  time in competitive conditions, an estimate for the predictive parameter, the
  purging coefficient ($d$), that quantifies the magnitude of genetic purging, either
  against overall inbreeding depression ($d \approx 0.3$), or against the component ascribed
  to nonlethal alleles ($d_{NL} \approx 0.2$). We find that competitive fitness declines at a high
  rate when inbreeding increases in the absence of purging. However, in moderate
  size populations under competitive conditions, inbreeding depression need not be
  too dramatic in the medium to short term, as the efficiency of purging is also
  very high. Furthermore, we find that purging occurred under competitive conditions
  also reduced the inbreeding depression that is expressed in the absence of
  competition.
authors:
- admin
- Eugenio López-Cortegano
date: "2016-06-15"
doi: "https://doi.org/10.1111/evo.12983"
featured: false
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Evolution*, 70(8)'
publication_short: ""
publication_types:
- "2"
publishDate: "2016-06-15"
summary: 我们估计了在竞争条件下 *Drosophila melanogaster* ($N_{e}\approx 50$) 的瓶颈种群的近交负荷和遗传清除的程度，并评估了 40 代以上的适应性进化。
tags:
- Drosophila melanogaster
- 基因清除
- 近亲繁殖
title: Estimation of genetic purging under competitive conditions
url_code: ""
url_dataset: ""
url_pdf: https://onlinelibrary.wiley.com/doi/epdf/10.1111/evo.12983
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
