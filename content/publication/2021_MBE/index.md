---
abstract: De novo mutations are central for evolution, since they provide the raw material for natural selection by regenerating
  genetic variation. However, studying de novo mutations is challenging and is generally restricted to model species, so we
  have a limited understanding of the evolution of the mutation rate and spectrum between closely related species. Here,
  we present a mutation accumulation (MA) experiment to study de novo mutation in the unicellular green alga
  *Chlamydomonas incerta* and perform comparative analyses with its closest known relative, *Chlamydomonas reinhardtii*.
  Using whole-genome sequencing data, we estimate that the median single nucleotide mutation (SNM) rate in *C. incerta* is
  $\mu = 7.6 \times 10^{-10}$, and is highly variable between MA lines, ranging from $\mu = 0.35 \times 10^{-10}$ to $\mu = 131.7 \times 10^{ -10}$. The SNM
  rate is strongly positively correlated with the mutation rate for insertions and deletions between lines ($r > 0.97$). We infer
  that the genomic factors associated with variation in the mutation rate are similar to those in *C. reinhardtii*, allowing for
  cross-prediction between species. Among these genomic factors, sequence context and complexity are more important
  than GC content. With the exception of a remarkably high C→T bias, the SNM spectrum differs markedly between the
  two *Chlamydomonas* species. Our results suggest that similar genomic and biological characteristics may result in a
  similar mutation rate in the two species, whereas the SNM spectrum has more freedom to diverge.
authors:
- admin
date: "2021-05-05"
doi: "https://doi.org/10.1093/molbev/msab140"
featured: false
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Molecular Biology and Evolution*, 38 (9): 3709-3723.'
publication_short: ""
publication_types:
- "2"
publishDate: "2021-05-05"
summary: A study of comparative mutability between *Chlamydomonas reinhardtii* and its closest known relative *C. incerta*, using WGS data from mutation accumulation lines maintained over nearly 1000 generations.
tags:
- Chlamydomonas reinhardtii
- Mutation
title: De novo mutation rate variation and its determinants in Chlamydomonas
url_code: ""
url_dataset: "https://www.ncbi.nlm.nih.gov/bioproject/?term=PRJNA687646"
url_pdf: "https://academic.oup.com/mbe/advance-article-pdf/doi/10.1093/molbev/msab140/38337628/msab140.pdf"
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
