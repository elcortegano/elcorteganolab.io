---
abstract: The inbreeding depression of fitness traits can be a major threat to the survival of populations
  experiencing inbreeding. However, its accurate prediction requires taking into account the genetic purging
  induced by inbreeding, which can be achieved using a “purged inbreeding coefficient”. We have developed
  a method to compute purged inbreeding at the individual level in pedigreed populations with
  overlapping generations. Furthermore, we derive the inbreeding depression slope for individual logarithmic
  fitness, which is larger than that for the logarithm of the population fitness average. In addition, we provide
  a new software, PURGd, based on these theoretical results that allows analyzing pedigree data to detect
  purging, and to estimate the purging coefficient, which is the parameter necessary to predict the joint
  consequences of inbreeding and purging. The software also calculates the purged inbreeding coefficient
  for each individual, as well as standard and ancestral inbreeding. Analysis of simulation data show that this
  software produces reasonably accurate estimates for the inbreeding depression rate and for the purging
  coefficient that are useful for predictive purposes.
authors:
- admin
- Eugenio López-Cortegano
date: "2016-09-01"
doi: "https://doi.org/10.1534/g3.116.032425"
featured: false
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*G3*, 6(11)'
publication_short: ""
publication_types:
- "2"
publishDate: "2016-09-01"
summary: An approximation to compute the purged inbreeding coefficient in pedigrees with overlapping generations is derived, and software to estimate the purging coefficient ($d$) in populations is presented (https://gitlab.com/elcortegano/PURGd).
tags:
- Genetic purging
- Inbreeding
- Software
title: Predictive model and software for inbreeding-purging analysis of pedigreed populations
url_code: https://gitlab.com/elcortegano/PURGd
url_dataset: ""
url_pdf: https://www.g3journal.org/content/ggg/6/11/3593.full.pdf
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
