---
abstract: For over a century, inbred mice have been used in many areas of genetics research to gain insight into the genetic variation
  underlying traits of interest. The generalizability of any genetic research study in inbred mice is dependent upon all
  individual mice being genetically identical, which in turn is dependent on the breeding designs of companies that supply
  inbred mice to researchers. Here, we compare whole-genome sequences from individuals of four commonly used inbred
  strains that were procured from either the colony nucleus or from a production colony (which can be as many as ten
  generations removed from the nucleus) of a large commercial breeder, in order to investigate the extent and nature of genetic
  variation within and between individuals. We found that individuals within strains are not isogenic, and there are differences
  in the levels of genetic variation that are explained by differences in the genetic distance from the colony nucleus. In
  addition, we employ a novel approach to mutation rate estimation based on the observed genetic variation and the expected
  site frequency spectrum at equilibrium, given a fully inbred breeding design. We find that it provides a reasonable per
  nucleotide mutation rate estimate when mice come from the colony nucleus ($\sim 7.9 \times 10^{-9}$ in C3H/HeN), but substantially
  inflated estimates when mice come from production colonies.
authors:
- admin
- Eugenio López-Cortegano
date: "2021-01-06"
doi: "https://doi.org/10.1038/s41437-020-00361-1"
featured: false
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Heredity*, 126'
publication_short: ""
publication_types:
- "2"
publishDate: "2021-01-06"
summary: 使用小鼠的 WGS 数据，我们展示了一种基于观察到的遗传变异和预期位点频谱的自交系突变率的新估计。
tags:
- Mus musculus
- 突变
title: "Inbred lab mice are not isogenic: genetic variation within inbred strains used to infer the mutation rate per nucleotide site"
url_code: ""
url_dataset: ""
url_pdf: https://www.nature.com/articles/s41437-020-00361-1.pdf
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
