---
abstract: Traditionally it has been suggested that sexual selection can cause sexual size dimorphism (SSD).
  However, a recent review in gastropods shows that SSD itself can also cause sexual selection (Ng et al.,
  2019. Animal Behaviour, 148, 53e62). This may be the case if mate choice exists, with males preferring to
  mate with females similar in body size but somewhat larger than themselves (female-biased preference).
  We formally investigated this verbal explanation by computer simulations using a Gaussian mating
  preference function. Parameters of that function were also estimated from empirical data. Our results
  suggest that sexual selection (estimated as selection differential) is strong when mate choice is high and
  exerted by only one of the sexes, being influenced by SSD and the magnitude of the female-biased
  preference. All these factors cause a negative relationship between SSD and the (sexual) selection dif-
  ferential, similar to that observed in Ng et al.‘s review on gastropods. Empirical estimates of male mate
  choice from wild-captured mating pairs of different gastropod species confirm that mate choice by males
  is biased towards females slightly larger than themselves. Our results also illustrate that if mate choice is
  truly involved in determining SSD, present-day sexual selection cannot be used to estimate the past
  magnitude of mate choice, as SSD influences present-day patterns of sexual selection.
authors:
- admin
- Eugenio López-Cortegano
date: "2020-01-29"
doi: "https://doi.org/10.1016/j.anbehav.2019.12.005"
featured: false
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Animal Behaviour*, 160'
publication_short: ""
publication_types:
- "2"
publishDate: "2020-01-29"
summary: 使用模拟种群，我们展示了当交配受到择偶和偏好偏见的影响时，性别大小二态性如何导致性选择。 因此，可能无法仅从性选择数据来估计实践中的配偶选择。 
tags:
- 计算机模拟
- 伴侣选择
- 性选择
title: Mate choice based on body size similarity in sexually dimorphicpopulations causes strong sexual selection
url_code: ""
url_dataset: ""
url_pdf: https://www.sciencedirect.com/science/article/pii/S000334721930394X/pdfft?md5=1923a536984be8fc59eed6d7006f966e&pid=1-s2.0-S000334721930394X-main.pdf
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
