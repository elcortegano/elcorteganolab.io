---
abstract: Inbreeding depression and genetic purging are important processes shaping the survivability and evolution of small populations. However, detecting purging is challenging in practice, in part because there are limited tools dedicated to it. I present a new R package to assist population analyses on detection and quantification of the inbreeding depression and genetic purging of biological fitness in pedigreed populations. It includes a collection of methods to estimate different measurements of inbreeding (Wright’s, partial and ancestral inbreeding coefficients) as well as purging parameters (purged inbreeding, and opportunity of purging coefficients). Additional functions are also included to estimate population parameters, allowing to contextualise inbreeding and purging these results in terms of the population demographic history. purgeR is a valuable tool to gain insight into processes related to inbreeding and purging, and to better understand fitness and inbreeding load evolution in small populations.
authors:
- admin
- Eugenio López-Cortegano
date: "2021-08-18"
doi: "https://doi.org/10.1093/bioinformatics/btab599"
featured: true
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Bioinformatics*, 38 (2): 564-565'
publication_short: ""
publication_types:
- "2"
publishDate: "2021-08-18"
summary: 用于近亲繁殖和纯种种群清除分析的 R 包。迄今为止最完整的遗传清除分析工具，包括首次估计个体近交负荷下降的模型。
tags:
- 基因清除
- 近亲繁殖
- 软件
title: "purgeR: Inbreeding and purging in pedigreed populations"
url_code: https://gitlab.com/elcortegano/purgeR
url_dataset: ""
url_pdf: https://academic.oup.com/bioinformatics/advance-article-pdf/doi/10.1093/bioinformatics/btab599/39785728/btab599.pdf
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
