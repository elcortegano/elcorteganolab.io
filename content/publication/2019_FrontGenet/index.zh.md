---
abstract: One of the main objectives of conservation programs is the maintenance of genetic diversity
  because this provides the adaptive potential of populations to face new environmental
  challenges. Genetic diversity is generally assessed by means of neutral molecular
  markers, and it is usually quantified by the expected heterozygosity under Hardy-Weinberg
  equilibrium and the number of alleles per locus or allelic diversity. These two measures
  of genetic diversity are complementary because whereas the former is directly related
  to genetic variance for quantitative traits and, therefore, to the short-term response to
  selection and adaptation, the latter is more sensitive to population bottlenecks and relates
  more strongly to the long-term capacity of adaptation of populations. In the context of
  structured populations undergoing conservation programs, it is important to decide the
  optimum management strategy to preserve as much genetic diversity as possible while
  avoiding inbreeding. Here we examine, through computer simulations, the consequences
  of choosing a conservation strategy based on maximizing either heterozygosity or allelic
  diversity of single-nucleotide polymorphism haplotypes in a subdivided population. Our
  results suggest that maximization of allelic diversity can be more efficient in maintaining the
  genetic diversity of subdivided populations than maximization of expected heterozygosity
  because the former maintains a larger number of alleles while making a better control of
  inbreeding. Thus, maximization of allelic diversity should be a recommended strategy in
  conservation programs for structured populations.
authors:
- admin
- Eugenio López-Cortegano
date: "2019-09-13"
doi: "https://doi.org/10.3389/fgene.2019.00843"
featured: false
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Frontiers in Genetics*, 10(843)'
publication_short: ""
publication_types:
- "2"
publishDate: "2019-09-13"
summary: "使用模拟种群通过两种不同的策略探索分散种群中的遗传多样性最大化：等位基因多样性的最大化或预期的杂合性。 我们的结果表明，增加等位基因多样性在减少种群近亲繁殖和保持种群适应潜力之间提供了更好的折衷。"
tags:
- 计算机模拟
- 遗传多样性
- 近亲繁殖
title: Optimal management of genetic diversity in subdivided populations
url_code: ""
url_dataset: ""
url_pdf: https://www.frontiersin.org/articles/10.3389/fgene.2019.00843/pdf
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
