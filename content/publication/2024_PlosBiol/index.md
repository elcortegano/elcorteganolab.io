---
abstract: Each generation, spontaneous mutations introduce heritable changes that tend to reduce fitness in populations of
  highly adapted living organisms. This erosion of fitness is countered by natural selection, which keeps deleterious mutations
  at low frequencies and ultimately removes most of them from the population. The classical way of studying the impact of
  spontaneous mutations is via mutation accumulation (MA) experiments, where lines of small effective population size are
  bred for many generations in conditions where natural selection is largely removed. Such experiments in microbes, invertebrates,
  and plants have generally demonstrated that fitness decays as a result of MA. However, the phenotypic consequences of MA
  in vertebrates are largely unknown, because no replicated MA experiment has previously been carried out. This gap in our
  knowledge is relevant for human populations, where societal changes have reduced the strength of natural selection, potentially
  allowing deleterious mutations to accumulate. Here, we study the impact of spontaneous MA on the mean and genetic variation
  for quantitative and fitness-related traits in the house mouse using the MA experimental design, with a cryopreserved
  control to account for environmental influences. We show that variation for morphological and life history traits accumulates
  at a sufficiently high rate to maintain genetic variation and selection response. Weight and tail length measures decrease
  significantly between 0.04% and 0.3% per generation with narrow confidence intervals. Fitness proxy measures (litter size
  and surviving offspring) decrease on average by about 0.2% per generation, but with confidence intervals overlapping zero.
  When extrapolated to humans, our results imply that the rate of fitness loss should not be of concern in the foreseeable future.

authors:
- admin
date: "2024-09-26"
doi: "https://doi.org/10.1371/journal.pbio.3002795"
featured: false
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*PLoS Biology*'
publication_short: ""
publication_types:
- "2"
publishDate: "2024-09-26"
summary: We investigate the phenotypic consequences of mutation accumulation in a large pedigree population of mice, demonstrating that new mutations exert only weak effects.
tags:
- Mus musculus
- Mutation
title: An estimate of fitness reduction from mutation accumulation in a mammal allows assessment of the consequences of relaxed selection
url_code: ""
url_dataset: ""
url_pdf: "https://journals.plos.org/plosbiology/article/file?id=10.1371/journal.pbio.3002795&type=printable"
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
