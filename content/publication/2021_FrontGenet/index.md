---
abstract: Most indigenous pig resources are known to originate from China. Thus, establishing
  conservation priorities for these local breeds is very essential, especially in the case
  of limited conservation funds. Therefore, in this study, we analyzed 445 individuals
  belonging to six indigenous breeds from the Taihu Lake Region, using a total of 131,300
  SNPs. In order to determine the long-term guidelines for the management of these
  breeds, we analyzed the level of diversity in the metapopulation following a partition of
  diversity within and between breed subpopulations, using both measures of genic and
  allelic diversity. From the study, we found that the middle Meishan (MMS) pig population
  contributes the most (22%) to the total gene diversity while the Jiaxing black (JX) pig
  population contributes the most (27%) to the gene diversity between subpopulations.
  Most importantly, when we consider one breed is removed from the meta-population,
  the first two breeds prioritized should be JX pig breed and Fengjing pig breed followed
  by small Meishan (SMS), Mizhu (MI), and Erhualian (EH) if we pay more attention to the
  gene diversity between subpopulations. However, if the priority focus is on the total gene
  diversity, then the first breed to be prioritized would be the Shawutou (SW) pig breed
  followed by JX, MI, EH, and Fengjing (FJ). Furthermore, we noted that if conservation
  priority is to be based on the allelic diversity between subpopulations, then the MI breed
  should be the most prioritized breed followed by SW, Erhuanlian, and MMS. Summarily,
  our data show that different breeds have different contributions to the gene and allelic
  diversity within subpopulations as well as between subpopulations. Our study provides
  a basis for setting conservation priorities for indigenous pig breeds with a focus on
  different priority criteria.
authors:
- admin
- Eugenio López-Cortegano
date: "2021-03-03"
doi: "https://doi.org/10.3389/fgene.2021.558873"
featured: false
image:
  focal_point: ""
  preview_only: false
projects: []
publication: '*Frontiers in Genetics*, 12: 558873'
publication_short: ""
publication_types:
- "2"
publishDate: "2021-03-03"
summary: We analyze how genic and allelic diversity distribute within and between populations of Taihu pig breeds, and provide maangement recommendations.
tags:
- Genetic diversity
- Sus scrofa
title: Conservation priorities analysis of Chinese indigenous pig breeds in the Taihu lake region
url_code: ""
url_dataset: ""
url_pdf: https://www.frontiersin.org/articles/10.3389/fgene.2021.558873/pdf
url_poster: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---
