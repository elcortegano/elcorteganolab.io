---
date: "2019-07-24"
external_link: https://gitlab.com/elcortegano/GWEHS
image:
  focal_point: Smart
links:
- icon: chart-bar
  icon_pack: fas
  name: app
  url: http://gwehs.uvigo.es
summary: 使用 NHGRI-EBI GWAS Catalog 数据的全基因组效应大小和遗传力筛选器。
tags:
- 遗传力
title: GWEHS
---
