---
date: "2019-07-24"
external_link: https://gitlab.com/elcortegano/GWEHS
image:
  focal_point: Smart
links:
- icon: chart-bar
  icon_pack: fas
  name: app
  url: http://gwehs.uvigo.es
summary: A genome-wide effect sizes and heritability screener using data from the NHGRI-EBI GWAS Catalog.
tags:
- Heritability
title: GWEHS
---
