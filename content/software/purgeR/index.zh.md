---
date: "2021-04-30"
external_link: https://gitlab.com/elcortegano/purgeR
image:
  focal_point: Smart
links:
- icon: r-project
  icon_pack: fab
  name: CRAN
  url: https://CRAN.R-project.org/package=purgeR
summary: 谱系种群中近交清除遗传参数的估计。
tags:
- 遗传多样性
- 基因清除
- 近亲繁殖
title: purgeR
---
