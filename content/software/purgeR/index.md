---
date: "2021-04-30"
external_link: https://gitlab.com/elcortegano/purgeR
image:
  focal_point: Smart
links:
- icon: r-project
  icon_pack: fab
  name: CRAN
  url: https://CRAN.R-project.org/package=purgeR
summary: Estimation of inbreeding-purging genetic parameters in pedigreed populations.
tags:
- Genetic diversity
- Genetic purging
- Inbreeding
title: purgeR
---
