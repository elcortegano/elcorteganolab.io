---
date: "2016-09-01"
external_link: https://gitlab.com/elcortegano/PURGd
image:
  focal_point: Smart
summary: A software to detect purging and to estimate inbreeding-purging genetic parameters in pedigreed populations.
tags:
- Genetic purging
- Inbreeding
title: PURGd
---
