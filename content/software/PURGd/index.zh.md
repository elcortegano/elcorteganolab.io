---
date: "2016-09-01"
external_link: https://gitlab.com/elcortegano/PURGd
image:
  focal_point: Smart
summary: 一种检测清除和估计系谱种群中近交清除遗传参数的软件。
tags:
- 基因清除
- 近亲繁殖
title: PURGd
---
