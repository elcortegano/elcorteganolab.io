---
date: "2019-04-02"
external_link: "https://gitlab.com/elcortegano/metapop2"
image:
  focal_point: Smart
summary: 细分种群的基因和等位基因多样性分析，以及保护计划中的管理工具。
tags:
- 遗传多样性
- 近亲繁殖
title: Metapop2
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
