---
date: "2019-04-02"
external_link: "https://gitlab.com/elcortegano/metapop2"
image:
  focal_point: Smart
summary: Analysis of gene and allelic diversity in subdivided populations, as well as a tool for management in conservation programs.
tags:
- Genetic diversity
- Inbreeding
title: Metapop2
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""
---
