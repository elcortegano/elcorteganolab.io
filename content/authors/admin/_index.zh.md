+++
# Display name
title = "Eugenio López-Cortegano"

# Username (this should match the folder name)
authors = ["admin"]

# Is this the primary user of the site?
superuser = true

# Role/position
role = "统计遗传学家"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
# organizations = [ { name = "爱丁堡大学", url = "http://www.homepages.ed.ac.uk/pkeightl/" } ]

# Short bio (displayed in user profile at end of posts)
bio = ""

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "elcortegano@protonmail.com"

# List (academic) interests or hobbies
interests = [
  "种群和数量遗传学",
  "基因组架构",
  "进化论",
  "保护遗传学",
  "计算生物学"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Admin"]

# List qualifications (such as academic degrees)
[[education.courses]]
  course = "人口遗传学博士"
  institution = "马德里康普顿斯大学"
  year = 2017

[[education.courses]]
  course = "农林生物技术理学硕士"
  institution = "马德里理工大学"
  year = 2012

[[education.courses]]
  course = "生物学理学士"
  institution = "马德里康普顿斯大学"
  year = 2011

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.

[[social]]
  icon = "rss"
  icon_pack = "fas"
  link = "https://elcortegano.gitlab.io/zh/index.xml"

[[social]]
  icon = "envelope"
  icon_pack = "fas"
  link = "mailto:elcortegano@protonmail.com"  # For a direct email link, use "mailto:test@example.org".

[[social]]
  icon = "home"
  icon_pack = "fas"
  link = "https://elcortegano.gitlab.io/zh/"

[[social]]
  icon = "weixin"
  icon_pack = "fab"
  link = "https://gitlab.com/elcortegano/elcortegano.gitlab.io/-/raw/master/content/authors/admin/wechat.jpg?ref_type=heads"

[[social]]
  icon = "orcid"
  icon_pack = "ai"
  link = "http://orcid.org/0000-0001-6914-6305"
  
[[social]]
  icon = "publons"
  icon_pack = "ai"
  link = "https://publons.com/a/1507595"

[[social]]
  icon = "gitlab"
  icon_pack = "fab"
  link = "https://gitlab.com/elcortegano"

#[[social]]
#  icon = "stackoverflow"
#  icon_pack = "ai"
#  link = "https://stackoverflow.com/users/3609394/elcortegano"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
#[[social]]
#  icon = "cv"
#  icon_pack = "ai"
#  link = "http://lcolladotor.github.io/cv/cv.pdf"

+++

我对遗传漂变，突变和共同选择作用下，小且有效种群中突变负荷的演化过程感兴趣。我制定的一个研究计划，能够研究种群突变率的快速变化，识别自然分离的诱变等位基因，并评估它们对生物适应度和基因组进化的影响。

{{< icon name="download" pack="fas" >}} **下载我的{{< staticref "https://gitlab.com/elcortegano/elcortegano.gitlab.io/-/jobs/artifacts/master/raw/output/cv.pdf?job=pdf" "newtab" >}}完整简历{{< /staticref >}}**（英文）。 
