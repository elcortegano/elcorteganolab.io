+++
# Display name
title = "Eugenio López-Cortegano"

# Username (this should match the folder name)
authors = ["admin"]

# Is this the primary user of the site?
superuser = true

# Role/position
role = "Statistical Geneticist"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
# organizations = [ { name = "University of Edinburgh", url = "http://www.homepages.ed.ac.uk/pkeightl/" } ]

# Short bio (displayed in user profile at end of posts)
bio = ""

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "elcortegano@protonmail.com"

# List (academic) interests or hobbies
interests = [
  "Population and Quantitative genetics",
  "Genome architecture",
  "Evolutionary theory",
  "Conservation genetics",
  "Computational biology"
]

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups = ["Admin"]

# List qualifications (such as academic degrees)
[[education.courses]]
  course = "PhD in Population Genetics"
  institution = "Complutense University of Madrid"
  year = 2017

[[education.courses]]
  course = "MSc in Agroforestal Biotechnology"
  institution = "Technical University of Madrid"
  year = 2012

[[education.courses]]
  course = "BSc in Biology"
  institution = "Complutense University of Madrid"
  year = 2011

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.

[[social]]
  icon = "rss"
  icon_pack = "fas"
  link = "https://elcortegano.gitlab.io/index.xml"

[[social]]
  icon = "envelope"
  icon_pack = "fas"
  link = "mailto:elcortegano@protonmail.com"  # For a direct email link, use "mailto:test@example.org".

[[social]]
  icon = "home"
  icon_pack = "fas"
  link = "https://elcortegano.gitlab.io/"

[[social]]
  icon = "weixin"
  icon_pack = "fab"
  link = "https://gitlab.com/elcortegano/elcortegano.gitlab.io/-/raw/master/content/authors/admin/wechat.jpg?ref_type=heads"

[[social]]
  icon = "orcid"
  icon_pack = "ai"
  link = "http://orcid.org/0000-0001-6914-6305"
  
[[social]]
  icon = "publons"
  icon_pack = "ai"
  link = "https://publons.com/a/1507595"

[[social]]
  icon = "gitlab"
  icon_pack = "fab"
  link = "https://gitlab.com/elcortegano"

#[[social]]
#  icon = "stackoverflow"
#  icon_pack = "ai"
#  link = "https://stackoverflow.com/users/3609394/elcortegano"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
#[[social]]
#  icon = "cv"
#  icon_pack = "ai"
#  link = "http://lcolladotor.github.io/cv/cv.pdf"

+++

I am interested in the evolution of the mutational load in populations of small effective sizes under the joint action selection, genetic drift and mutation. I have developed a research programme with the objective to investigate rapid changes in population mutation rates, identifying naturally segregating mutator alleles, and evaluating their impact on biological fitness and genome evolution.

{{< icon name="download" pack="fas" >}} **Download my {{< staticref "https://gitlab.com/elcortegano/elcortegano.gitlab.io/-/jobs/artifacts/master/raw/output/cv.pdf?job=pdf" "newtab" >}}complete CV{{< /staticref >}}**.
