# CURRICULUM VITAE

### PERSONAL DETAILS

First name: Eugenio  
Last name: López Cortegano

Institute of Evolutionary Biology, School of Biological Sciences  
University of Edinburgh, Edinburgh EH9 3FL (United Kingdom)  
Cell #: +44 7821 552371  
Email: [e.lopez-cortegano@ed.ac.uk](mailto:e.lopez-cortegano@ed.ac.uk)  
Website: [https://elcortegano.gitlab.io/](https://elcortegano.gitlab.io)  

### EDUCATION AND ACADEMIC POSITIONS

Postdoctoral Research Associate  
School of Biological Sciences  
University of Edinburgh  
Peter Keightley, Ph.D., Mentor  
September 2019 to present  

Postdoctoral Fellow  
Department of Biochemistry, Genetics and Immunology  
University of Vigo  
Armando Caballero, Ph.D., Mentor  
June 2017 to August 2019  

Ph.D., Biology  
Complutense University of Madrid  
Received May 2017  
Awarded with *cum laude*.  
Dissertation: *Evaluation of genetic purging in small sized populations*  
Aurora García-Dorado, Ph.D., Advisor  

MSc, Agroforestal Biotechnology  
Technical University of Madrid  
Received August 2012  

BSc, Biology  
Complutense University of Madrid  
Received July 2011  

### PUBLICATIONS

[ORCID ID: 0000-0001-6914-6305](https://orcid.org/0000-0001-6914-6305)

2021\. **López-Cortegano E**, Craig RJ, Chebib J, Samuels T, Morgan AD, Kraemer SA, Böndel KB, Ness RW, Colegrave N, Keightley PD. De novo mutation rate variation and its determinants in Chlamydomonas. *Molecular Biology and Evolution*, 38 (9): 3709-3723.

2021\. **López-Cortegano E**. purgeR: Inbreeding and purging in pedigreed populations. *Bioinformatics*, https://doi.org/10.1093/bioinformatics/btab599.  

2021\. **López-Cortegano E**, Moreno E, García-Dorado A. Genetic purging in captive endangered ungulates with extremely low effective population sizes. *Heredity*, https://doi.org/10.1038/s41437-021-00473-2.  

2021\. Novo I, **López-Cortegano E**, Caballero A. Highly pleiotropic variants of human traits are enriched in genomic regions with strong background selection. *Human Genetics*, doi: https://doi.org/10.1007/s00439-021-02308-w.  

2021\. Zhao QB, **López-Cortegano E**, Oyelami FO, Zhang Z, Ma PP, Wang QS, Pan YC. Conservation priorities analysis of Chinese indigenous pig breeds in the Taihu Lake Region. *Frontiers in Genetics*, 12: 558873.  

2021\. Pérez-Pereira N, Pouso R, Rus A, Vilas A, **López-Cortegano E**, García-Dorado A, Quesada H, Caballero A. Long-term exhaustion of the inbreeding load in Drosophila melanogaster. *Heredity*, doi: https://doi.org/10.1038/s41437-021-00464-3  

2021\. Chebib J, Jackson BC, **López-Cortegano E**, Tautz D, Keightley PD. Inbred lab mice are not isogenic: genetic variation within inbred strains used to infer the mutation rate per nucleotide site. *Heredity*, 126(1): 107-116.  

2020\. **López-Cortegano E**, Carpena-Catoira C, Carvajal-Rodríguez A, and Rolán-Alvarez E. Mate choice based on body size similarity in sexually dimorphic populations causes strong sexual selection. *Animal Behavior*, 160: 69-78.  

2019\. **López-Cortegano E**, Pérez-Figueroa A, and Caballero A. Metapop2: re-implementation of software for the analysis and management of subdivided populations using gene and allelic diversity. *Molecular Ecology Resources*, 19 (4): 1095-1100.  

2019\. **López-Cortegano E**, and Caballero A. Inferring the nature of missing heritability in human traits using data from the GWAS Catalog. *Genetics*, 212 (3): 891-904.  

2019\. **López-Cortegano E**, Pouso R, Labrador A, Pérez-Figueroa A, Fernández J, and Caballero A. Optimal management of genetic diversity in subdivided populations. *Frontiers in Genetics*, 10: 843.  

2019\. **López-Cortegano E**, and Caballero A. GWEHS: A Genome-Wide Effect sizes and Heritability Screener. *Genes*, 10 (8): 558.  

2018\. **López-Cortegano E**, Bersabé D, Wang J, and García-Dorado A. Detection of genetic purging and predictive value of purging parameters estimated in pedigreed populations. *Heredity*, 121: 38-51.  

2016\. García-Dorado A, Wang J, and **López-Cortegano E.** Predictive model and software for inbreeding-purging analysis of pedigreed populations. *G3: Genes, Genomes, Genetics*, 6 (11): 3593-3601.  

2016\. **López-Cortegano E**, Vilas A, Caballero A, and García-Dorado A. Estimation of genetic purging under competitive conditions. *Evolution*, 70 (8): 1856-1870.  

### SOFTWARE DEVELOPED

I have developed software for quantitative and population analysis in C++ and R (visit: [https://gitlab.com/elcortegano](https://gitlab.com/elcortegano)).

- **Metapop2**: Analysis of gene and allelic diversity in subdivided populations, as well as a tool for management in conservation programs.

- **purgeR**: Estimation of inbreeding-purging genetic parameters in pedigreed populations (R package 'purgeR' in CRAN).

- **GWEHS**: A Genome-Wide Effect sizes and Heritability Screener (interactive website: http://gwehs.uvigo.es).

- **PURGd**: A software to estimate inbreeding-purging genetic parameters in pedigreed populations.

### TEACHING AND MENTORING

I am certified as Associate professor by the Spanish government, have more than 200 hours of teaching experience, and mentor 7 MSc and BSc Honours projects.

**University of Edinburgh**

Teacher in Population and Quantitative Genetics (10 hours)  
MSc in Quantitative Genetics and Genome Analysis  
2021-2022  

Tutoring thesis for the MSc in Quantitative Genetics  
- 2020-2021: "Detection and prediction of the effects of de novo mutations on fitness in Chlamydomonas"  

**University of Vigo**

Teacher in Quantitative Genetics (30 hours)  
MSc in Genomics and Genetics  
2018-2021  

Tutoring thesis for the MSc in Genomics and Genetics  
- 2020-2021: "Study of the nature of missing heritability in complex human traits from data obtained in genomic association studies"  
- 2018-2019: "Analysis of the heterogeneity of the effective population size in the human genome"  

Tutoring honours projects for the BSc in Biology  
- 2018-2019: "Computational analysis of the heritability estimation bias using familial information"  
- 2018-2019: "Regional analysis of diversity in the genome"  
- 2017-2018: "Analysis of the genomic position of SNPs found in GWAS for human traits"  
- 2017-2018: "Evaluation of fitness and inbreeding depression in purged lines of *Drosophila melanogaster*"  

**Complutense University of Madrid**  

Practical sessions of Evolutionary Biology (120 hours)  
BSc in Biology  
2014-2017  

Practical sessions of Conservation Biology (66 hours)  
BSc in Biology  
2014-2017  

### PUBLISHED ORAL/POSTER PRESENTATIONS

2022\. 55th Population Genetics Group Meeting  
- **Oral presentation**: **López-Cortegano E**, Craig RJ, Chebib J, Ness RW, Colegrave N, Keightley PD. *The rate and spectra of de novo structural mutations in Chlamydomonas reinhardtii*.  
Norwich (United Kingdom, online)  

2021\. 5th Uppsala Transposon Symposium  
- **Flash talk**: **López-Cortegano E**, Craig RJ, Chebib J, Ness RW, Colegrave N, Keightley PD. *The rate and spectra of transposition and other structural mutations in Chlamydomonas*.  
Uppsala (Sweden, online)  

2021\. 54th Population Genetics Group Meeting   
- **Oral presentation**: **López-Cortegano E**, Craig RJ, Chebib J, Samuels T, Morgan AD, Kraemer SA, Böndel KB, Ness RW, Colegrave N, Keightley PD. *Nature and variability of de novo mutations in Chlamydomonas*.  
Liverpool (United Kingdom, online)  

2021\. XLII Meeting of the Spanish Society of Genetics  
- **Flash talk**: **López-Cortegano E**, Craig RJ, Chebib J, Samuels T, Morgan AD, Kraemer SA, Böndel KB, Ness RW, Colegrave N, Keightley PD. *Evolution of mutational properties in phylogenetically closely related species*.  
- **Poster presentation**: Pérez-Pereira N, Pouso R, Rus A, Vilas A, **López-Cortegano E**, García-Dorado A, Quesada H, Caballero A. *Long-term evaluation of the inbreeding load in populations of Drosophila melanogaster*.  
- **Poster presentation**: Novo I, **López-Cortegano E**, Caballero A. *Strong background selection on highly pleiotropic variants for human traits and diseases*.  
Madrid (Spain, online)  

2020\. International Conference of Quantitative Genetics 6  
- **Poster presentation**: **López-Cortegano E**. *Minimum number of generations to start detecting purging*.  
Brisbane (Australia, online)  

2020\. VII Meeting of the Spanish Society of Evolutionary Biology  
- **Poster presentation**: Pérez-Pereira N, **López-Cortegano E**, García-Dorado A, Caballero A. *Detection of purging and estimation of predictive parameters in populations under different management systems*.  
Sevilla (Spain)  

2020\. 53rd Population Genetics Group Meeting  
- **Oral presentation**: **López-Cortegano E**, Craig RJ, Chebib J, Morgan AD, Ness RW, Colegrave N, and Keightley PD. *A first look to the de novo mutation rate in Chlamydomonas incerta*.  
Leicester (United Kingdom)  

2019\. 70th Annual Meeting of the European Federation of Animal Science  
-  **Oral presentation**: **López-Cortegano E**, Pouso R, Pérez-Figueroa A, Fernández J, and Caballero A. *Optimal management of gene and allelic diversity in subdivided populations*.  
Ghent (Belgium)  

2018\. XXII Population Genetics and Evolution Seminar  
- **Oral presentation**: **López-Cortegano E**, and Caballero A. *The missing heritability problem: how many variants are left to find?*.  
Aranjuez (Spain)  

2018\. II Annual Meeting CINBIO  
- **Poster presentation**: **López-Cortegano E**, and Caballero A. *Inferring the nature of missing heritability in human diseases*.  
Vigo (Spain)  

2018\. VI Meeting of the Spanish Society for Evolutionary Biology  
- **Poster presentation**: **López-Cortegano E**, Bersabé D, Wang J, and García-Dorado A. *Detection of genetic purging and predictive value of purging parameters estimated in pedigreed populations*.  
Palma de Mallorca (Spain)  

2016\. XXI Population Genetics and Evolution Seminar.  
- **Oral presentation**: García-Dorado A, Wang J, and **López-Cortegano E**. *Detection and estimation of genetic purging using genealogical information*.  
Sitges (Spain)  

2014\. XX Population Genetics and Evolution Seminar  
- **Oral presentation**: **López-Cortegano E**, and García-Dorado A. *Evaluation of the intensity of purging and its consequences on the fitness of threatened populations*.  
Granada (Spain)  

### EDITORIAL CONTRIBUTIONS

Publons: [https://publons.com/a/1507595](https://publons.com/a/1507595)  

I am part of the reviewers board of *Frontiers in Genetics* (2021-Present), *Journal of Evolutionary Biology* (2020-Present) and *Animals* (2020-2021), and have participated in the peer-review process of 41 scientific articles belonging to 11 different international journals:

- Animals (x18)
- Journal of Evolutionary Biology (x7)
- Frontiers in Genetics (x5)
- Genes (x4)
- Human Genetics (x2)
- Nature Human Behaviour (x2)
- Plants (x2)
- Diversity (x1)
- Evolutionary Applications (x1)
- Genetica (x1)
- International Journal of Environmental Research and Public Health (x1)

### LABORATORY EXPERIENCE

- DNA extraction (including for long-read sequencing).
- Maintenance and culture of Drosophila fruit flies, green algae (Chlamydomonas), and fungal species.
- Use of basic laboratory procedures: PCR, cloning, sequencing, cryopreservation, etc.

### ADMINISTRATIVE EXPERIENCE

I am a certified systems administrator by the Linux Professional Institute (LPIC-1), and have experience managing high performance computing clusters.

I also have experience in sourcing, planning and contracting genomic services, in particular PacBio CLR and CCS (HiFi) sequencing projects.

### RESEARCH GRANTS

UEECA Scholarship  
Union of Spanish Entities of Animal Science  
To attend the 2019 European Federation of Animal Science (EAAP) Annual Meeting in Ghent (Belgium)  

Predoctoral research grant (BES-2012-055006)  
National Programme for Training Human Resources  
Ministry for the Economy and Competitiveness, Spanish Government  
Aurora García-Dorado, Ph.D., Advisor  
December 2012 to November 2016  

Collaboration scholarship  
Department of Forest and Pasture Management. Technical University of Madrid  
Luis Gil, Ph.D., Advisor  
October 2011 to June 2012  
July 2011 to August 2011  
June 2010 to December 2010  
July 2009 to August 2009  

### RESEARCH STAYS IN OTHER CENTERS

Faculty of Biology, University of Vigo (Spain)  
Armando Caballero, Ph.D., Mentor  
April 2016 to June 2016  

Institute of Zoology, Zoological Society of London (United Kingdom)  
Jinliang Wang, Ph.D., Mentor  
February 2015 to May 2015  

### PROFESSIONAL MEMBERSHIPS AND SERVICE

Peer-review 1 Research Grant Proposal at the MRC (UK)  
2019  

Genetics Society (UK)  
September 2019 to present  

FlyBase Community Advisory Group  
September 2014 to present  

Spanish Genetics Society  
October 2012 to present  

### COURSES, WORKSHOPS AND CERTIFICATIONS

Long-read bioinformatics  
Edinburgh Genomics  

Writing and Presenting Scientific Papers  
European Association of Animal Science (EEAP/CUP)  

Prediction of complex traits  
Technical University of Valencia  

Phylogenies and Genealogies of DNA: Reconstruction and applications  
University of Barcelona  

Advanced programming in C++  
National University of Distance Education  

LPIC-1 Linux Server Professional  
Linux Professional Institute  

Regression models  
Johns Hopkins Bloomberg School of Public Health  

Data mining  
University of Illinois at Urbana-Champaign  

Introduction to Graph Theory  
University of California San Diego & National Research University Higher School of Economics  

### LANGUAGE SKILLS

- Spanish: mother language

- English: proficient user (writing and speaking)

- French: upper-intermediate (DELF B2)

- Portuguese: upper-intermediate (Intermediário Superior B2, CELPE-Bras)

- Chinese: elementary level (HSK2)
